import { Box, Button, Grid, InputLabel, TextField, Typography } from "@mui/material";
import React from "react";
import DashboardCard from "../components/shared/DashboardCard";
import Link from "next/link";

const Categories = () => {
  return (
    <div>
      <DashboardCard title="Categories">
        <Box sx={{ minHeight: "60vh" }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexDirection: { xs: "column", md: "row" },
                  justifyContent: "space-between",
                }}
              >
                <InputLabel shrink={false} htmlFor={"username"}>
                  <Typography>Category Name*</Typography>
                </InputLabel>
                <TextField
                  id="category"
                  sx={{ width: { xs: "100%", md: "80%" }, marginTop: { xs: 2, md: 0 } }}
                  margin="normal"
                  name="category"
                  autoFocus
                  placeholder="Product Name"
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-end",
                  gap: 2,
                  flexDirection: { xs: "column", sm: "row" },
                }}
              >
                <Button variant="outlined" sx={{ minWidth: "100px" }}>Cancel</Button>
                <Button variant="contained" sx={{ minWidth: "100px" }}>Save</Button>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </DashboardCard>
    </div>
  );
};

export default Categories;
