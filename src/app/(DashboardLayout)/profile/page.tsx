"use client";

import React, { useState, ChangeEvent } from 'react';
import {
  Box,
  Container,
  Typography,
  Avatar,
  IconButton,
  TextField,
  Button,
  Grid,
  Paper
} from '@mui/material';
import { Edit } from '@mui/icons-material';
import { styled } from '@mui/system';
import DashboardCard from '../components/shared/DashboardCard';

const ProfileImageWrapper = styled(Box)({
  position: 'relative',
  display: 'inline-block',
  '&:hover': {
    '& .edit-icon': {
      opacity: 1,
    },
  },
});

const EditIconWrapper = styled('label')({
  position: 'absolute',
  bottom: 0,
  right: 0,
  opacity: 0,
  transition: 'opacity 0.3s',
  cursor: 'pointer'
});

const ProfilePage = () => {
  const [selectedImage, setSelectedImage] = useState<string | null>(null);
  const [initialProfile, setInitialProfile] = useState({
    name: 'John Doe',
    email: 'johndoe@example.com',
    phone: '+123456789',
    company: 'Example Inc.'
  });
  const [profile, setProfile] = useState(initialProfile);
  const [initialPasswords, setInitialPasswords] = useState({
    currentPassword: '',
    newPassword: '',
    confirmPassword: ''
  });
  const [passwords, setPasswords] = useState(initialPasswords);
  const [showProfileButtons, setShowProfileButtons] = useState(false);
  const [showPasswordButtons, setShowPasswordButtons] = useState(false);

  const handleImageChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e) => {
        if (e.target) {
          setSelectedImage(e.target.result as string);
          setShowProfileButtons(true);
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const handleProfileChange = (field: string) => (event: ChangeEvent<HTMLInputElement>) => {
    setProfile({ ...profile, [field]: event.target.value });
    setShowProfileButtons(true);
  };

  const handlePasswordsChange = (field: string) => (event: ChangeEvent<HTMLInputElement>) => {
    setPasswords({ ...passwords, [field]: event.target.value });
    setShowPasswordButtons(true);
  };

  const handleCancelProfileChanges = () => {
    setProfile(initialProfile);
    setSelectedImage(null);
    setShowProfileButtons(false);
  };

  const handleUpdateProfile = () => {
    // Implement profile update logic here
    setInitialProfile(profile);
    setShowProfileButtons(false);
  };

  const handleCancelPasswordChanges = () => {
    setPasswords(initialPasswords);
    setShowPasswordButtons(false);
  };

  const handleUpdatePassword = () => {
    // Implement password update logic here
    setInitialPasswords(passwords);
    setShowPasswordButtons(false);
  };

  return (
    <DashboardCard>
      <Box>
      <Box display="flex" justifyContent="center" mb={4}>
        <ProfileImageWrapper>
          <Avatar
            src={selectedImage || '/default-profile.png'}
            alt="Profile Image"
            sx={{ width: 150, height: 150 }}
          />
          <EditIconWrapper className="edit-icon">
            <Edit />
            <input
              type="file"
              accept="image/*"
              onChange={handleImageChange}
              hidden
            />
          </EditIconWrapper>
        </ProfileImageWrapper>
      </Box>
      <Typography variant="h6" gutterBottom>
        Profile Details
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            label="Name"
            variant="outlined"
            value={profile.name}
            onChange={handleProfileChange('name')}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            label="Email"
            variant="outlined"
            value={profile.email}
            onChange={handleProfileChange('email')}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            label="Phone Number"
            variant="outlined"
            value={profile.phone}
            onChange={handleProfileChange('phone')}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth
            label="Company Name"
            variant="outlined"
            value={profile.company}
            onChange={handleProfileChange('company')}
          />
        </Grid>
      </Grid>
      {showProfileButtons && (
        <Box mt={3} display="flex" justifyContent="flex-end" gap={2}>
          <Button variant="contained" color="primary" onClick={handleUpdateProfile}>
            Update Profile
          </Button>
          <Button variant="outlined" color="secondary" onClick={handleCancelProfileChanges}>
            Cancel
          </Button>
        </Box>
      )}
      <Box mt={5}>
        <Typography variant="h6" gutterBottom>
          Update Password
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              fullWidth
              label="Current Password"
              type="password"
              variant="outlined"
              value={passwords.currentPassword}
              onChange={handlePasswordsChange('currentPassword')}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              label="New Password"
              type="password"
              variant="outlined"
              value={passwords.newPassword}
              onChange={handlePasswordsChange('newPassword')}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              label="Confirm New Password"
              type="password"
              variant="outlined"
              value={passwords.confirmPassword}
              onChange={handlePasswordsChange('confirmPassword')}
            />
          </Grid>
        </Grid>
        {showPasswordButtons && (
          <Box mt={3} display="flex" justifyContent="flex-end" gap={2}>
            <Button variant="contained" color="primary" onClick={handleUpdatePassword}>
              Update Password
            </Button>
            <Button variant="outlined" color="secondary" onClick={handleCancelPasswordChanges}>
              Cancel
            </Button>
          </Box>
        )}
      </Box></Box>
    </DashboardCard>
  );
};

export default ProfilePage;
