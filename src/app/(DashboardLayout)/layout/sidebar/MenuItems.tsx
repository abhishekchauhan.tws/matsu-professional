import { IconHomeCheck } from "@tabler/icons-react";
import { IconBarcode, IconLayoutDashboard, IconShoppingBag } from "@tabler/icons-react";

import { uniqueId } from "lodash";

const Menuitems = [
  {
    navlabel: true,
    subheader: " ",
  },

  {
    id: uniqueId(),
    title: "Dashboard",
    icon: IconLayoutDashboard,
    href: "/",
  },
  {
    id: uniqueId(),
    title: "Products",
    icon: IconShoppingBag,
    href: "/products",
  },
  {
    id: uniqueId(),
    title: "Stock Tracking",
    icon: IconHomeCheck,
    href: "/stock-tracking",
  },
  // {
  //   id: uniqueId(),
  //   title: "Categories",
  //   icon: IconCopy,
  //   href: "/categories",
  // },
  {
    id: uniqueId(),
    title: "Barcode Reader",
    icon: IconBarcode,
    href: "/barcode-reader",
  },
];

export default Menuitems;
