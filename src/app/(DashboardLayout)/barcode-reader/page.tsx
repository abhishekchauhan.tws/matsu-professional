import { Box, FormControl, Grid, MenuItem, Select, TextField, Button } from '@mui/material'
import React from 'react'
import DashboardCard from '../components/shared/DashboardCard'

const Barcode = () => {
  return (
    <div>
      <DashboardCard title="Barcode Reader">
        <Box sx={{ minHeight: "50vh" }}></Box>
      </DashboardCard>
      <Box sx={{ overflow: "auto", paddingTop: 2 }}>
        <Grid container rowSpacing={2} columnSpacing={{ xs: 2, sm: 2, md: 1 }}>
          <Grid item xs={12} md={5} sx={{ display: 'flex', alignItems: 'left' }}>
            <TextField
              fullWidth
              placeholder='Please Enter Barcode'
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} md={2} sx={{ display: 'flex', alignItems: 'left' }}>
            <TextField
              fullWidth
              placeholder='Piece'
              variant="outlined"
            />
          </Grid>
          <Grid item xs={6} sm={6} md={1.5} lg={1.5} sx={{ display: 'flex', justifyContent: { xs: 'center', sm: 'flex-start', md: 'flex-start' }, alignItems: 'center' }}>
            <Button variant="outlined" sx={{
              background: "rgb(229 229 229)", border: "none", color: "rgb(97 97 97)", borderRadius: "4px", minWidth: { xs: "140px", sm: "120px" }, minHeight: "50px", fontWeight: 'bold', '&:hover': {
                background: "rgb(229 229 229)",
                border: "none",
              }
            }}>
              Interest
            </Button>
          </Grid>
          <Grid item xs={6} sm={6} md={1.5} lg={1.5} sx={{ display: 'flex', justifyContent: { xs: 'center', sm: 'flex-start', md: 'flex-start' }, alignItems: 'center' }}>
            <Button variant="outlined" sx={{
              background: "rgb(229 229 229)", border: "none", color: "rgb(97 97 97)", borderRadius: "4px", minWidth: { xs: "140px", sm: "120px" }, minHeight: "50px", fontWeight: 'bold', '&:hover': {
                background: "rgb(229 229 229)",
                border: "none",
              }
            }}>
              Add
            </Button>
          </Grid>
          <Grid item xs={12} md={2} sx={{ display: 'flex', alignItems: 'left' }}>
            <FormControl fullWidth>
              <Select
                defaultValue=""
                variant="outlined"
                displayEmpty
              >
                <MenuItem value="" disabled>Select a Category</MenuItem>
                <MenuItem value="electronics">Cebtral Warehoue</MenuItem>
                <MenuItem value="furniture">Remaining Deposit</MenuItem>
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </Box>
    </div>
  )
}

export default Barcode