'use client'
import { styled, Box, FormControl, Grid, MenuItem, Checkbox, Button, Paper, Select, TextField, Typography, RadioGroup, FormControlLabel, Radio } from '@mui/material'
import React, { useRef, useState } from 'react'
import DashboardCard from '../components/shared/DashboardCard'



const AddProduct = () => {
  const [stockTracking, setStockTracking] = useState('done'); // Initial state for stock tracking radio buttons
  const fileInputRef: any = useRef(null);
  const [fileName, setFileName] = useState('');

  const handleButtonClick = () => {
    fileInputRef.current.click();
  };

  const HiddenInput: any = styled('input')({
    display: 'none',
  });

  const handleStockTrackingChange = (event: any) => {
    setStockTracking(event.target.value);
  };

  const handleFileChange = (event: any) => {
    const file = event.target.files[0];
    if (file) {
      setFileName(file.name);
    }
  };
  return (
    <div>
      <DashboardCard title="Add Product Details">
        <Box sx={{ overflow: "auto", padding: 2 }}>
          <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'left' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Product Name*</Typography>
              <TextField
                fullWidth
                placeholder='Product Name'
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'left' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Category*</Typography>
              <FormControl fullWidth>
                <Select
                  defaultValue=""
                  variant="outlined"
                  displayEmpty
                >
                  <MenuItem value="" disabled>Select a Category</MenuItem>
                  <MenuItem value="electronics">Electronics</MenuItem>
                  <MenuItem value="furniture">Furniture</MenuItem>
                  <MenuItem value="clothing">Clothing</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'left' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Product Identification Code</Typography>
              <TextField
                fullWidth
                placeholder='Product Identification Code'
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={6} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
              <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Barkod*</Typography>
              <TextField
                fullWidth
                placeholder='Barcode Number'
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={6} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
              <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Stock Code*</Typography>
              <TextField
                fullWidth
                placeholder='Stock Code'
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={6} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
              <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Buying/Selling Unit*</Typography>
              <FormControl fullWidth>
                <Select
                  defaultValue=""
                  variant="outlined"
                  displayEmpty
                >
                  <MenuItem value="" disabled>Piece</MenuItem>
                  <MenuItem value="electronics">Carrier Bag</MenuItem>
                  <MenuItem value="furniture">Palette</MenuItem>
                  <MenuItem value="clothing">Litre</MenuItem>
                  <MenuItem value="clothing">File</MenuItem>
                  <MenuItem value="clothing">Gram</MenuItem>
                  <MenuItem value="clothing">Kilogram</MenuItem>
                  <MenuItem value="clothing">Ton</MenuItem>
                  <MenuItem value="clothing">Day</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
              <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Size/Weight</Typography>
              <TextField
                fullWidth
                placeholder='Dimensions'
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={6} sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Stock Tracking*</Typography>
              <FormControl component="fieldset" fullWidth  >
                <RadioGroup sx={{ display: "flex", flexDirection: "row", flexWrap: "nowrap" }} aria-label="stock-tracking" name="stock-tracking" value={stockTracking} onChange={handleStockTrackingChange}>
                  <FormControlLabel value="done" control={<Radio />} label="let it be done" />
                  <FormControlLabel value="not-done" control={<Radio />} label="don't do it" />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography sx={{ minWidth: 180, textAlign: 'left', marginRight: 2, fontWeight: 'bold' }}>The amount of stock*</Typography>
              <TextField
                fullWidth
                placeholder='0'
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Critical Stock Tracking</Typography>
              <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", marginRight: 2 }}>
                <Checkbox checked={true} />
                <Typography sx={{ fontWeight: 'bold' }}>Active</Typography>
              </Box>
              <TextField fullWidth variant="outlined" />
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>VAT Rate*
                *</Typography>
              <FormControl fullWidth>
                <Select
                  defaultValue=""
                  variant="outlined"
                  displayEmpty
                >
                  <MenuItem value="" disabled>20%</MenuItem>
                  <MenuItem value="electronics">10%</MenuItem>
                  <MenuItem value="furniture">8%</MenuItem>
                  <MenuItem value="furniture">1%</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
              <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Purchase Price Including Taxes*</Typography>
              <TextField
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={6} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
              <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Sales Price Including Taxes*</Typography>
              <TextField
                fullWidth
                placeholder='Sales Price Including Taxes'
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Product Photo</Typography>
              <TextField
                fullWidth
                variant="outlined"
                id="product-photo-display"
                sx={{ textAlign: "center", cursor: "pointer" }}
                placeholder='Click to upload file'
                value={fileName}
                onClick={handleButtonClick}
                InputProps={{
                  readOnly: true,
                }}
              />
              <HiddenInput
                ref={fileInputRef}
                sx={{ textAlign: "center" }}
                type="file"
                id="product-photo"
                onChange={handleFileChange}
              />
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Warehouse Information*</Typography>
              <FormControl fullWidth>
                <Select
                  defaultValue=""
                  variant="outlined"
                  displayEmpty
                >
                  <MenuItem value="" disabled>Select a Category</MenuItem>
                  <MenuItem value="electronics">Cebtral Warehoue</MenuItem>
                  <MenuItem value="furniture">Remaining Deposit</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Shelf Information</Typography>
              <TextField
                fullWidth
                variant="outlined"
                placeholder='Shelf Information'
              />
            </Grid>
            <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
              <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Desi Information</Typography>
              <TextField
                fullWidth
                variant="outlined"
                placeholder='Desi Information'
              />
            </Grid>
          </Grid>
          <Box sx={{ marginTop: 2, display: "flex", justifyContent: "right", gap: 2 }}>
            <Button variant="outlined">
              Give Up
            </Button>
            <Button variant="contained">
              Save
            </Button>
          </Box>
        </Box>
      </DashboardCard></div>
  )
}

export default AddProduct;