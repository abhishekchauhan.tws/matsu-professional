'use client'
import { Box, FormControl, Grid, MenuItem, Checkbox, Button, Paper, Select, TextField, Typography, RadioGroup, FormControlLabel, Radio } from '@mui/material'
import React, { useState } from 'react'
import DashboardCard from '../components/shared/DashboardCard'

const AddManuel = () => {
    const [stockTracking, setStockTracking] = useState('done'); // Initial state for stock tracking radio buttons

    const handleStockTrackingChange = (event: any) => {
        setStockTracking(event.target.value);
    };

    return (
        <div>
            <DashboardCard title="Add Manuel Details">
                <Box sx={{ overflow: "auto", padding: 2 }}>
                    <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'left' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Customer*:</Typography>
                            <TextField
                                fullWidth
                                placeholder='Customer'
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'left' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Customer Address:</Typography>
                            <TextField
                                fullWidth
                                placeholder='Customer Address'
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'left' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Customer TIN:</Typography>
                            <TextField
                                fullWidth
                                placeholder='Customer TIN:'
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Customization Number:
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Customization Number'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Scenario: </Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Scenario'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Delivery Note Type:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Delivery Note Type'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Waybill number:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Waybill number:'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Delivery Note Time:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Delivery Note Time:'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>The shipment date:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='The shipment date'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Shipment Time:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Shipment Time'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Order number:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Order number'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Order date:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Order date:'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <Grid item xs={12} md={6} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Waybill date:</Typography>
                            </Grid>
                            <Grid item xs={12} md={6} >
                                <TextField
                                    fullWidth
                                    placeholder='Waybill date'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>

                        <Grid item xs={12} md={8} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left' }}>
                            <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Operation type*:</Typography>
                            <FormControl fullWidth>
                                <Select
                                    defaultValue=""
                                    variant="outlined"
                                    displayEmpty
                                >
                                    <MenuItem value="" disabled>Purchase</MenuItem>
                                    <MenuItem value="electronics"> Purchase</MenuItem>
                                    <MenuItem value="furniture">Sales</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'left', gap: 2 }}>
                            <Grid item xs={0} md={2} sx={{ display: 'flex', alignItems: 'center', justifyContent: "center", gap: 2 }} ></Grid>
                            <Grid item xs={7} md={7} sx={{ display: 'flex', alignItems: 'center', justifyContent: "center" }} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Product Barcode Number:*</Typography>
                                <FormControl fullWidth>
                                    <Select
                                        defaultValue=""
                                        variant="outlined"
                                        displayEmpty
                                    >
                                        <MenuItem value="" disabled>Product Barcode Number</MenuItem>
                                        <MenuItem value="electronics"> Purchases</MenuItem>
                                        <MenuItem value="furniture">Sales</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={5} md={3} sx={{ display: 'flex', alignItems: 'center', justifyContent: "center", gap: 1 }} >
                                <Typography sx={{ fontWeight: 'bold', marginBottom: 1 }}>Piece:</Typography>
                                <TextField
                                    fullWidth
                                    placeholder='Piece'
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Storage*:</Typography>
                            <FormControl fullWidth>
                                <Select
                                    defaultValue=""
                                    variant="outlined"
                                    displayEmpty
                                    placeholder="Depo"
                                >
                                    <MenuItem value="electronics">Cebtral Warehoue</MenuItem>
                                    <MenuItem value="furniture">Remaining Deposit</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Driver Information:</Typography>
                            <TextField
                                fullWidth
                                variant="outlined"
                                placeholder='Chauffeur'
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Vehicle and Trailer License Plate:</Typography>
                            <TextField
                                fullWidth
                                variant="outlined"
                                placeholder='Vehicle and Trailer License Plate'
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Delivery address:</Typography>
                            <TextField
                                fullWidth
                                variant="outlined"
                                placeholder='Delivery address'
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Remarks/Notes:</Typography>
                            <TextField
                                fullWidth
                                variant="outlined"
                                placeholder='Notes'
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sx={{ display: 'flex', alignItems: 'center' }}>
                            <Typography sx={{ minWidth: 180, marginRight: 2, fontWeight: 'bold' }}>Delivered by:</Typography>
                            <TextField
                                fullWidth
                                variant="outlined"
                                placeholder='Submitted by'
                            />
                        </Grid>
                    </Grid>
                    <Box sx={{ marginTop: 3, display: "flex", justifyContent: "right", gap: 2 }}>
                        {/* <Button variant="outlined" sx={{
                            color: "rgb(97 97 97)", borderRadius: "4px", minWidth: { xs: "140px", sm: "120px" }, minHeight: "50px", fontWeight: 'bold',
                        }}>
                            Give Up
                        </Button>
                        <Button variant="outlined" sx={{
                            background: "rgb(229 229 229)", border: "none", color: "rgb(97 97 97)", borderRadius: "4px", minWidth: { xs: "140px", sm: "120px" }, minHeight: "50px", fontWeight: 'bold', '&:hover': {
                                background: "rgb(229 229 229)",
                                border: "none",
                            }
                        }}>
                            Save
                        </Button> */}
                          <Button variant="outlined">
              Give Up
            </Button>
            <Button variant="contained">
              Save
            </Button>
                    </Box>
                </Box>
            </DashboardCard></div>
    )
}

export default AddManuel;