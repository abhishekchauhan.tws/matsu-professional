import { createTheme } from "@mui/material/styles";

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: "#050A44",
    },
    secondary: {
      main: "#141619",
    },
    error: {
      main: "#fb977d",
    },
  },
});

export default theme;
