import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import toast from 'react-hot-toast';

const initialState = {
  isLoading: false,
  profileData: {},
  LoginDetailData: {},
};

const BASE_URL = 'http://3.141.236.121:8000';
export const globalSlice = createSlice({
  name: 'globalSlice',
  initialState,
  reducers: {
    SetLoading: (state, actions) => {
      state.isLoading = actions.payload;
    },
    LoginDetails: (state, actions) => {
      state.LoginDetailData = actions.payload;
    },
    profiledata: (state, actions) => {
      state.profileData = actions.payload;
    },
   
  },
});

export const {
  SetLoading,
  profiledata,
  LoginDetails,
 
} = globalSlice.actions;

export default globalSlice.reducer;

// export function LoginUser({ loginData, navigate }) {
//   return async (dispatch) => {
//     try {
//       dispatch(SetLoading(true));
//       const response = await axios.post(`${BASE_URL}/api/customer-login`, loginData);
//       if (response?.data?.status === 200) {
//         dispatch(SetLoading(false));
//         dispatch(LoginDetails(response?.data));
//         localStorage.setItem('token', response?.data?.token);
//         localStorage.setItem('username', loginData?.username);
//         localStorage.setItem('role', response?.data?.user_type);
//         toast.success(response?.data?.message);
//         if (response.data.user_type === 'user') {
//           navigate('/dashboard');
//         } else {
//           navigate('/admin/dashboard');
//         }
//         return response?.data;
//       } else {
//         dispatch(SetLoading(false));
//         toast.error(response?.data?.message);
//       }
//     } catch (error) {
//       toast.error("Something went wrong.");
//       dispatch(SetLoading(false));
//     }
//   };
// }

// export function RegisterUser({ registerData, navigate }) {
//   return async (dispatch) => {
//     try {
//       dispatch(SetLoading(true));
//       const response = await axios.post(`${BASE_URL}/api/customer-register`, registerData);
//       if (response?.status === 201) {
//         toast.success(response?.data?.message);
//         dispatch(SetLoading(false));
//         navigate('/auth/login');
//         return response?.data;
//       }
//     } catch (error) {
//       toast.error("Something went wrong.");
//       dispatch(SetLoading(false));
//     } 
//   };
// }

// export function profileget() {
//   return async (dispatch) => {
//     let token = localStorage.getItem('token');
//     const requestOptions = {
//       headers: {
//         Authorization: `Token ${token}`,
//       },
//     };
//     try {
//       dispatch(SetLoading(true));
//       const response = await axios.get(`${BASE_URL}/api/user-profile`, requestOptions);
//       if (response?.data) {
//         dispatch(SetLoading(false));
//         dispatch(profiledata(response?.data.data));
//         return response?.data;
//       }
//     } catch (error) {
//       toast.error("Something went wrong.");
//       dispatch(SetLoading(false));
//     }
//   };
// }

