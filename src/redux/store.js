import { configureStore } from '@reduxjs/toolkit';
import counterSlice from "./authslice"


export const store = configureStore({
    reducer:
        { counter: counterSlice }

})
